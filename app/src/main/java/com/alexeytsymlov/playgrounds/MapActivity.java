package com.alexeytsymlov.playgrounds;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.utils.GeoPoint;

public class MapActivity extends AppCompatActivity {
    private final GeoPoint KALUGA_POSITION = new GeoPoint(54.5293, 36.27542);
    private final GeoPoint START_POSITION = KALUGA_POSITION;
    private final int START_ZOOM = 12;

    private MapView mMapView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        configureMap();
    }

    private void configureMap() {
        mMapView = (MapView) findViewById(R.id.map);
        mMapView.showBuiltInScreenButtons(true);
        mMapView.showScaleView(false);
        MapController mMapController = mMapView.getMapController();
        mMapController.setPositionNoAnimationTo(START_POSITION);
        mMapController.setZoomCurrent(START_ZOOM);
    }

    public void onClearButtonClick(View view) {
        clearSearchText();
    }

    private void clearSearchText() {
        EditText search_text = (EditText)findViewById(R.id.search);
        search_text.setText("");
    }

    public void onSettingsClick(View view) {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }
}
